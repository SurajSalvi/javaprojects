/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.giplTraining.arithmeticoperations;

import java.util.Scanner;

/**
 *
 * @author ANKIT SALVI
 */
public class ArithmeticOperations {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc =new Scanner (System.in);
        int a,b;
        System.out.println("Enter 1st number ");
        a=sc.nextInt();
        System.out.println("Enter 2nd number ");
        b=sc.nextInt();
        
        int sum=a+b;
        int sub=a-b;
        float mul=a*b;
        float div=a/b;
        int mod=a%b;
        
        System.out.println("addition is: "+sum);
        System.out.println("Subtraction is: "+sub);
        System.out.println("Multiplication is: "+mul);
        System.out.println("Division is: "+div);
        System.out.println("Modulo is: "+mod);
        
        
    }
    
}
