/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.giplTraining.arithmeticfunctions;

/**
 *
 * @author ANKIT SALVI
 */
public class ArithmeticFunctions {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //ArithmeticFunctions a=new ArithmeticFunctions();
        System.out.println(add(5,2));
        System.out.println(sub(5,3)); 
        System.out.println(mul(5,3));
        System.out.println(div(20,5));
        System.out.println(mod(10,3));
    }
  
    public static int add(int a,int b){
        return a+b;
    }
    public static int sub(int a,int b){
        return a-b;
    }
    public static float mul(float a,float b){
        return a*b;
    }
    public static float div(float a,float b){
        return a/b;
    }
    public static float mod(float a,float b){
        return a%b;
    }
    
    
}
