/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.giplTraining.printtable;

/**
 *
 * @author ANKIT SALVI
 */
public class PrintTable {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int n;
        for(int i=1;i<=10;i++){
            n=i;                        
            for (int j = 1; j <= 10; j++) {
                System.out.print("\t\t"+j*n);
            }
            System.out.println("");
        }
           
    }
    
}
