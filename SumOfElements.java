/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.giplTraining.sumofelements;

/**
 *
 * @author ANKIT SALVI
 */
public class SumOfElements {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int [] numbers={5,8,6,4,9,-2};
        int sum=0;
        for(int n:numbers){
            sum+=n;
        }
        System.out.println("Sum of array Elements is: "+sum);
    }
    
}
