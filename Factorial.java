/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.giplTraining.factorial;

import java.util.Scanner;

/**
 *
 * @author ANKIT SALVI
 */
public class Factorial {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter number: ");
        int n=sc.nextInt();
        int fact=1;
        int i=1;
        while(i <= n) {
            fact=fact*i;  
            i++;
        }
        System.out.println(fact);
    }
    
}
