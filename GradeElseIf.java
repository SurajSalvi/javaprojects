/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.giplTraining.gradeelseif;

import java.util.Scanner;

/**
 *
 * @author ANKIT SALVI
 */
public class GradeElseIf {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter your Marks: ");
        int percentage=sc.nextInt();
        
        if(percentage >= 80){
            System.out.println("Grade A");          
        }else if(percentage < 80 && percentage >=70){
            System.out.println("Grade B");
        }else if(percentage < 70 && percentage >=60){
            System.out.println("Grade c");
        }else if(percentage < 60 && percentage >=50){
            System.out.println("Grade D");
        }else if(percentage < 50 && percentage >=35){
            System.out.println("Grade E");
        }else{
            System.out.println("Failed !!!!");
        }
    }
}
